import React from 'react';
import "./Person.css";

const Person = (props) => {
    return (
        <div className="Person">
            <h2 onClick={props.click}>I am a {props.name} and I am {props.age} years old</h2>
            <h4>{props.children}</h4>
            <input type="text" onChange={props.changed} value={props.name} />
        </div>
    );
};

export default Person;